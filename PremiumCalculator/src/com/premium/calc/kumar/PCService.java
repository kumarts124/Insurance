package com.premium.calc.kumar;

public class PCService {
	
	public double calculatePremium ( ClientBean c)
	{
		double premium = 5000; 
		String name = c.getName();
		int age = c.getAge();
		String gender = c.getGender();
		String hypertension = c.getHypertension();
		String bloodpressure = c.getBloodpressure();
		String bloodsugar = c.getBloodsugar();
		String overweight = c.getOverweight();
		String smoking = c.getSmoking();
		String alcohol = c.getAlcohol();
		String dailyexercise = c.getDailyexercise();
		String drugs = c.getDrugs();
		
		// if above 18 years 
		
		
		
		
	    if (age >= 18) {
	        premium *= 1.1;
	    }
	    if (age >= 25) {
	        premium *= 1.1;
	    }
	    if (age >= 30) {
	        premium *= 1.1;
	    }
	    if (age >= 35) {
	        premium *= 1.1;
	    }
	    if (age >= 40) {
	        // Add 20% per 5 years above 40
	        int temp_age = age - 40;
	        while (temp_age >= 5) {
	            premium *= 1.2;
	            temp_age -= 5;
	        }
	    }

	    // Gender
	    if (gender.equals("Male")) {
	        premium *= 1.02;
	    }

	    // 1% increase per health issue
	    if(hypertension.contains("Yes")){
	        premium *= 1.01;
	    }
	    
	    if(bloodpressure.contains("Yes")){
	        premium *= 1.01;
	    }
	    if(bloodsugar.contains("Yes")){
	        premium *= 1.01;
	    }
	    
	    if(overweight.contains("Yes")){
	        premium *= 1.01;
	    }

	    
	    // Good Habit 3% reduction
	     
	        if (dailyexercise.equals("Yes")) {
	            premium *= 1.03;
	        }
	    
	    // Bad Habit decrement 3%
	    
	        if (smoking.equals("Yes"))
	        {
	            premium *= 0.97;
	        }
	        if (alcohol.equals("Yes"))
	        {
	            premium *= 0.97;
	        }
	        if (drugs.equals("Yes")) 
	        {
	            premium *= 0.97;
	        }
	        
	    return premium ;
	}

}
