package com.premium.calc.kumar;

import java.util.Scanner;

public class StartApp {

	public static void main(String[] args) {
		
		Scanner sc1 = new Scanner(System.in);
		
		System.out.println("Name :");
		String name = sc1.next();
		System.out.println("Gender :");
		String gender = sc1.next();
		System.out.println("Age :");
		int age = sc1.nextInt();
		System.out.println("Current Health :");
		System.out.println("Hypertension (Yes/No)");
		String hypertension  = sc1.next();
		System.out.println("Blood Sugar(Yes/No)");
		String bloodsugar  = sc1.next();
		System.out.println("Blood Pressure (Yes/No)");
		String bloodpressure  = sc1.next();
		System.out.println("Overweight (Yes/No)");
		String overweight  = sc1.next();
		
		System.out.println("Habits :");
		System.out.println("Smoking (Yes/No)");
		String smoking  = sc1.next();
		System.out.println("Alcohol (Yes/No)");
		String alcohol  = sc1.next();
		System.out.println("Daily Exercise (Yes/No)");
		String dailyexercise = sc1.next();
		System.out.println("Drugs (Yes/No)");
		String drugs = sc1.next();
		
		
		// Write to Bean 
		ClientBean cb = new ClientBean();
		cb.setName(name);
		cb.setAge(age);
		cb.setGender(gender);
		cb.setHypertension(hypertension);
		cb.setBloodpressure(bloodpressure);
		cb.setBloodsugar(bloodsugar);
		cb.setOverweight(overweight);
		cb.setSmoking(smoking);
		cb.setAlcohol(alcohol);
		cb.setDailyexercise(dailyexercise);
        cb.setDrugs(drugs);	
        
        // Send Bean to Service for Business logic part 
        PCService blogic = new PCService();
        
        double result = blogic.calculatePremium(cb);
        
        System.out.println("Health Insurance premium for Mr"+cb.getName()+" is "+result);

	}

}
