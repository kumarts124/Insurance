package com.premium.calc.kumar;

import java.io.Serializable;

public class ClientBean implements Serializable{
	
	private String name;
	private String gender;
	private int age;
	
	private String hypertension;
	private String bloodpressure;
	private String bloodsugar;
	private String overweight;
	
	private String smoking;
	private String alcohol;
	private String dailyexercise;
	private String drugs;
	public ClientBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ClientBean(String name, String gender, int age, String hypertension, String bloodpressure, String bloodsugar,
			String overweight, String smoking, String alcohol, String dailyexercise, String drugs) {
		super();
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.hypertension = hypertension;
		this.bloodpressure = bloodpressure;
		this.bloodsugar = bloodsugar;
		this.overweight = overweight;
		this.smoking = smoking;
		this.alcohol = alcohol;
		this.dailyexercise = dailyexercise;
		this.drugs = drugs;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getHypertension() {
		return hypertension;
	}
	public void setHypertension(String hypertension) {
		this.hypertension = hypertension;
	}
	public String getBloodpressure() {
		return bloodpressure;
	}
	public void setBloodpressure(String bloodpressure) {
		this.bloodpressure = bloodpressure;
	}
	public String getBloodsugar() {
		return bloodsugar;
	}
	public void setBloodsugar(String bloodsugar) {
		this.bloodsugar = bloodsugar;
	}
	public String getOverweight() {
		return overweight;
	}
	public void setOverweight(String overweight) {
		this.overweight = overweight;
	}
	public String getSmoking() {
		return smoking;
	}
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	public String getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(String alcohol) {
		this.alcohol = alcohol;
	}
	public String getDailyexercise() {
		return dailyexercise;
	}
	public void setDailyexercise(String dailyexercise) {
		this.dailyexercise = dailyexercise;
	}
	public String getDrugs() {
		return drugs;
	}
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + ((alcohol == null) ? 0 : alcohol.hashCode());
		result = prime * result + ((bloodpressure == null) ? 0 : bloodpressure.hashCode());
		result = prime * result + ((bloodsugar == null) ? 0 : bloodsugar.hashCode());
		result = prime * result + ((dailyexercise == null) ? 0 : dailyexercise.hashCode());
		result = prime * result + ((drugs == null) ? 0 : drugs.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((hypertension == null) ? 0 : hypertension.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((overweight == null) ? 0 : overweight.hashCode());
		result = prime * result + ((smoking == null) ? 0 : smoking.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientBean other = (ClientBean) obj;
		if (age != other.age)
			return false;
		if (alcohol == null) {
			if (other.alcohol != null)
				return false;
		} else if (!alcohol.equals(other.alcohol))
			return false;
		if (bloodpressure == null) {
			if (other.bloodpressure != null)
				return false;
		} else if (!bloodpressure.equals(other.bloodpressure))
			return false;
		if (bloodsugar == null) {
			if (other.bloodsugar != null)
				return false;
		} else if (!bloodsugar.equals(other.bloodsugar))
			return false;
		if (dailyexercise == null) {
			if (other.dailyexercise != null)
				return false;
		} else if (!dailyexercise.equals(other.dailyexercise))
			return false;
		if (drugs == null) {
			if (other.drugs != null)
				return false;
		} else if (!drugs.equals(other.drugs))
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (hypertension == null) {
			if (other.hypertension != null)
				return false;
		} else if (!hypertension.equals(other.hypertension))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (overweight == null) {
			if (other.overweight != null)
				return false;
		} else if (!overweight.equals(other.overweight))
			return false;
		if (smoking == null) {
			if (other.smoking != null)
				return false;
		} else if (!smoking.equals(other.smoking))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ClientBean [name=" + name + ", gender=" + gender + ", age=" + age + ", hypertension=" + hypertension
				+ ", bloodpressure=" + bloodpressure + ", bloodsugar=" + bloodsugar + ", overweight=" + overweight
				+ ", smoking=" + smoking + ", alcohol=" + alcohol + ", dailyexercise=" + dailyexercise + ", drugs="
				+ drugs + "]";
	}
	
	
	
	

}
